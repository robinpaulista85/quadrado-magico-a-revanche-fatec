/*Atividade 03 - Quadrado mágico a revanche

Implemente em C, o reconhecimento de números existentes seguindo dois processos distintos:

* O primeiro baseia-se na cópia dos valores digitados pelo usuário para um vetor, o qual deverá ser ordenado e posteriormente passará um processo de verificação sobre a possibilidade de valores repetidos.
* O segundo método baseia-se em um método chamado de janela deslizante e assim, um termo da matriz é definido como pivot e esse elemento é verificado com todos os valores existentes na matriz em busca de valores repetidos. Ao terminar o processo de verificação, caso, não encontre nenhum repetição, é possível afirma que o sistema é um quadrado mágico.

Importante:
* Ao encontrar um valor repetido, é necessário parar a busca uma vez, que naturalmente já não será possível ser um quadrado mágico. */

#include <stdlib.h>
#include <stdio.h>

int main () {
    int  i, j, m, n, resposta;

    do {
        printf ("Insira as dimensões da matriz:\n");
        scanf ("%d %d", &m, &n);
    }while (m != n);

    int matriz[m][n], somaLinha[n], somaColuna[m], somaDiagonalPrim = 0, somaDiagonalSec = 0;
    for (i = 0; i < m; i++){
        somaLinha[i] = 0;
        somaColuna[i] = 0;
    }

    printf ("Digite os elementos: \n");

    for (i = 0; i < m; i++){
        for (j = 0; j < n; j++){
            scanf ("%d", &matriz[i][j]);
        }
    }

    for (i = 0; i < m; i++) {
        for (j = 0; j < n; j++){
            somaLinha[i] += matriz[i][j];
        }
    }

    for (i = 0; i < n; i++) {
        for (j = 0; j < m; j++){
            somaColuna[i] += matriz[j][i];
        }
    }

    for (i=0; i < m; i++) {
        somaDiagonalPrim += matriz[i][i];
    }

    for (i = 0; i < m; i++) {
        for(j = 0; j < m; j++){
            if(j == (m - 1) -i){
            somaDiagonalSec += matriz[i][j];
            }
        }
    }
    
    resposta = 1;

    if (somaDiagonalPrim != somaDiagonalSec){
        resposta = 0;
    }

    for (i=0; i < m; i++){
        if (somaLinha[i] != somaColuna[i]){
            resposta = 0;
        }
    }

    for (i = 0; i < m - 1; i++){
        if (somaLinha[i] != somaLinha[i+1] || somaColuna[i] != somaColuna[i+1]){
            resposta = 0;
        }
    }

    if (somaLinha[0] != somaDiagonalPrim){
        resposta = 0;
    }if (resposta == 0){
        printf ("Não é um quadrado mágico!\n");
    }else {
        printf ("Esse é um quadrado mágico!\n");
    }

    system ("PAUSE");
    return 0;
}